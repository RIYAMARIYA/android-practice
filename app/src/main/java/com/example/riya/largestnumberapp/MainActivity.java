package com.example.riya.largestnumberapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    private EditText et1;
    private Button btres;
    private TextView tv_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addListenerOnButton();
    }

    public void addListenerOnButton() {
        et1 = (EditText) findViewById(R.id.editText);
        btres = (Button) findViewById(R.id.button);
        tv_data = (TextView) findViewById(R.id.textView);

    }

    public void onClick(View v) {
        String value1 = et1.getText().toString();
        int n = value1.length();
        int c[] = new int[n];
        int count = 0;
        int flag=0;
        int max;
        Pattern p = Pattern.compile("(([0-9]+[,' ']))");
        Matcher m = p.matcher(value1);
        boolean d = m.find();

        String[] a = value1.split("[,' ']");
        try {
                    for (int i = 0; i < a.length; i++)
                    {
                    if (a[i] != null)
                    {
                        count++;
                    }
                    if(a[0].equals(a[i]))
                    {
                        flag=0;
                    }
                    else
                    {
                        flag=1;
                    }
                    c[i] = Integer.parseInt(a[i]);

                    }


            if((count>=2)&&(flag==1))
            {
                if (d == true) {


                    max = c[0];

                    for (int i = 0; i < a.length; i++) {
                        if (max < c[i]) {
                            max = c[i];


                        }

                    }


                    tv_data.setText("Largest Number is" + " " + Integer.toString(max));
                }
            }
            if(flag==0)
            {
                tv_data.setText("All the numbers are same Nothing to find");
            }
            if(count<2)
            {
                tv_data.setText("Input should contains atleast 2 numbers");
            }

        }
        catch (NumberFormatException e) {
            tv_data.setText("Invalid input");
        }

    }
}











